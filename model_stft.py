from itertools import product

import numpy as np
import torch
from torch import nn
from torch.autograd import Variable

COMPRESSION = 0.3
eps = 1e-15


def my_istft(stft, hop_length, len_y):
    n_fft = 2 * (stft.shape[1] - 1)
    n_frames = stft.shape[2]

    buffer_y = torch.zeros(stft.shape[0], len_y + n_fft, dtype=torch.float32)
    buffer_y = buffer_y.cuda() if stft.is_cuda else buffer_y

    y_tmps = torch.irfft(stft.transpose(1, 2), signal_ndim=1, signal_sizes=(n_fft,))

    for i in range(n_frames):
        sample = i * hop_length
        y_tmp = y_tmps[:, i, :]
        buffer_y[:, sample:(sample + n_fft)] = buffer_y[:, sample:(sample + n_fft)] + y_tmp

    _y = buffer_y[:, int(n_fft // 2):-int(n_fft // 2)] / 2
    return _y


def complex_multiply(x, y):
    # x: (B, 2, F, T)
    a = x[:, 0].unsqueeze(1)
    b = x[:, 1].unsqueeze(1)
    c = y[:, 0].unsqueeze(1)
    d = y[:, 1].unsqueeze(1)

    real = a * c - b * d
    imag = a * d + b * c
    return torch.cat([real, imag], 1)


# noinspection PyArgumentList
class LayerNorm(nn.Module):
    def __init__(self, dimension, trainable=True):
        super(LayerNorm, self).__init__()

        if trainable:
            self.gain = nn.Parameter(torch.ones(1, dimension, 1, 1))
            self.bias = nn.Parameter(torch.zeros(1, dimension, 1, 1))
        else:
            self.gain = Variable(torch.ones(1, dimension, 1, 1), requires_grad=False)
            self.bias = Variable(torch.zeros(1, dimension, 1, 1), requires_grad=False)

    def forward(self, inp):
        # input size: (b, ch, F, T)
        mean = torch.mean(inp, 3, keepdim=True)
        std = torch.sqrt(torch.var(inp, 3, keepdim=True) + eps)

        x = (inp - mean.expand_as(inp)) / std.expand_as(inp)
        return x * self.gain.expand_as(x).type(x.type()) + self.bias.expand_as(x).type(x.type())


class DepthConv2d(nn.Module):
    def __init__(self, input_channel, hidden_channel, kernel,
                 dilation=(1, 1), stride=(1, 1), padding=(0, 0), causal=False):
        super(DepthConv2d, self).__init__()

        self.padding = padding

        self.linear = nn.Conv2d(input_channel, hidden_channel, (1, 1))
        if causal:
            self.conv1d = CausalConv2d(hidden_channel, hidden_channel, kernel_size=kernel,
                                       stride=stride,
                                       dilation=dilation, pad1=self.padding[0])
        else:
            self.conv1d = nn.Conv2d(hidden_channel, hidden_channel, kernel_size=kernel,
                                    stride=stride, padding=self.padding,
                                    dilation=dilation)

        self.BN = nn.Conv2d(hidden_channel, input_channel, (1, 1))

        self.nonlinearity1 = nn.PReLU()
        self.nonlinearity2 = nn.PReLU()

        self.reg1 = LayerNorm(hidden_channel)
        self.reg2 = LayerNorm(hidden_channel)

    def forward(self, inp):
        output = self.reg1(self.nonlinearity1(self.linear(inp)))
        output = self.reg2(self.nonlinearity2(self.conv1d(output)))
        output = self.BN(output)

        return output


class CausalConv2d(torch.nn.Conv2d):
    def __init__(self, in_channels, out_channels, pad1, **kwargs):
        self.__padding = (pad1, (kwargs.get("kernel_size", (3, 2))[1] - 1) * kwargs.get("dilation", (1, 1))[1])
        super(CausalConv2d, self).__init__(in_channels, out_channels, padding=self.__padding, **kwargs)

    def forward(self, inp):
        result = super(CausalConv2d, self).forward(inp)
        if self.__padding[1] != 0:
            return result[:, :, :, :-self.__padding[1]]
        return result


# noinspection PyTypeChecker
class InfoComMask(nn.Module):
    def __init__(self, n_fft=256, hop=125, bn_ch=32, sep_ch=64, kernel=(3, 3), causal=False, layers=6,
                 stacks=2, verbose=True, use_pit=False):
        super(InfoComMask, self).__init__()
        if verbose:
            print(f"#\t nfft = {n_fft}")

        self.n_fft = (n_fft // 2 + 1)
        self.FFT = n_fft
        self.HOP = hop
        self.BN_channel = bn_ch
        self.conv_channel = sep_ch
        self.kernel = kernel
        self.conv_pad = (int(np.log2((self.kernel[0] - 1) / 2)), int(np.log2((self.kernel[1] - 1) / 2)))

        self.enc_LN = LayerNorm(2)
        self.BN = nn.Conv2d(2, self.BN_channel, (1, 1))

        self.use_pit = use_pit

        self.layer = layers
        self.stack = stacks

        self.receptive_field_time = 0
        self.receptive_field_freq = 0
        self.conv = nn.ModuleList([])
        for s in range(self.stack):
            for i in range(self.layer):
                self.conv.append(DepthConv2d(self.BN_channel + 1, self.conv_channel,
                                             self.kernel, dilation=(2 ** i, 2 ** i), causal=causal,
                                             padding=(2 ** (i + self.conv_pad[0]), 2 ** (i + self.conv_pad[1]))))
                if s == 0 and i == 0:
                    self.receptive_field_time += self.kernel[0]
                    self.receptive_field_freq += self.kernel[1]
                else:
                    self.receptive_field_time += (self.kernel[0] - 1) * 2 ** i
                    self.receptive_field_freq += (self.kernel[1] - 1) * 2 ** i

        if verbose:
            print('Receptive field TIME: {:1d} samples.'.format(self.receptive_field_time))
            print('Receptive field FREQ: {:1d} samples.'.format(self.receptive_field_freq))

        self.reshape_env = nn.Sequential(nn.Conv1d(1, self.n_fft, 1))

        self.reshape = nn.Sequential(nn.Conv2d(self.BN_channel + 1, 2, (1, 1)), nn.Tanh())
        if self.use_pit:
            self.reshape2 = nn.Sequential(nn.Conv2d(self.BN_channel + 1, 2, (1, 1)), nn.Tanh())

    def forward(self, x, verbose=False):
        _x, env = x

        win = torch.hann_window(self.FFT)
        if _x.is_cuda:
            win = win.cuda()

        # input shape: B, T
        x_stft = torch.stft(_x, self.FFT, self.HOP, window=win)  # (B, F, L, 2)

        x_stft = compress(x_stft, COMPRESSION)
        x_stft = x_stft.permute(0, 3, 1, 2)  # (B, 2, F, L)

        x_stft_bn = self.BN(self.enc_LN(x_stft))  # (B, BN, F, L)
        env = env.unsqueeze(1)  # B, 1, L

        # new
        enc_env = self.reshape_env(env).unsqueeze(1)  # B, 1, F, L
        feat = torch.cat([x_stft_bn, enc_env], 1)  # B, BN + 1, F, L
        # mask estimation

        this_input = feat
        skip_connection = 0.
        for i in range(len(self.conv)):
            this_output = self.conv[i](this_input)
            skip_connection = skip_connection + this_output
            this_input = this_input + this_output

        mask = self.reshape(skip_connection)  # B, 2, F, T

        masked = complex_multiply(x_stft, mask).permute(0, 2, 3, 1)  # B, F, T, 2
        masked = compress(masked, 1. / COMPRESSION)
        output = my_istft(masked, hop_length=self.HOP, len_y=_x.shape[1])  # B, T
        if not self.use_pit:
            if verbose:
                return output, mask
            else:
                return output
        else:
            mask2 = self.reshape2(skip_connection)  # B, 2, F, T
            masked2 = complex_multiply(x_stft, mask2).permute(0, 2, 3, 1)  # B, F, T, 2
            masked2 = compress(masked2, 1. / COMPRESSION)
            output2 = my_istft(masked2, hop_length=self.HOP, len_y=_x.shape[1])  # B, T
            return output, output2


def compress(x, compression=0.3):
    # x: b, F, T, 2
    mag = torch.sqrt(x[..., 0] ** 2 + x[..., 1] ** 2 + 1e-15)
    ang = torch.atan2(x[..., 1], x[..., 0])
    re = (mag ** compression) * torch.cos(ang)
    im = (mag ** compression) * torch.sin(ang)
    return torch.cat([re.unsqueeze(3), im.unsqueeze(3)], 3)


def rec_field(stacks, layers, kernel):
    r = 1
    for _, l in product(range(stacks), range(layers)):
        r += (kernel - 1) * (2 ** l)
    return r
