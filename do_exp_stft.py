import argparse
import datetime
import json
import os

import numpy as np
import torch
import torch.nn as nn
import torch.optim as optim
from torch.utils.data import DataLoader

import data_utils
from logger import Logger
from model_stft import InfoComMask
from train_test import train, test

data_dir = './'
code_dir = './'
base_dir = code_dir + ''
training_data_path = data_dir + 'simple_dataset.h5'
validation_data_path = data_dir + 'simple_dataset.h5'


def main(args):
    print("#" * 99)
    num_gpu = torch.cuda.device_count()
    # fill more info in the name
    exp_name = datetime.datetime.now().strftime('%G%m%H%M%S_')
    exp_name += args.exp_name
    exp_name += "_{:02d}".format(int(args.noise_train * 10))
    exp_name += '_{}'.format(args.frame_train)
    exp_name += '_{}'.format(args.env)
    exp_name += '_{}'.format(args.hop)
    if args.bab == 1:
        exp_name += '_bab'
    else:
        exp_name += '_sep'

    if args.causal == 1:
        exp_name += '_C'
    else:
        exp_name += '_NC'

    logdir = f"{base_dir}/logs2/{exp_name}"

    if not os.path.isdir(logdir):
        os.mkdir(logdir)

    logger = Logger(logdir)
    print(f"#\tLog directory = {logdir}")
    val_save = f"{logdir}/model_weights.pt"

    # global params
    if args.cuda:
        torch.cuda.manual_seed(args.seed)
        kwargs = {'num_workers': 2, 'pin_memory': True}
    else:
        kwargs = {}

    # loaders
    print("#\tPreparing Loaders...")

    loader = data_utils.DatasetSTFTv5 if not args.bab else data_utils.DatasetSTFTv5BAB

    train_loader = DataLoader(loader(training_data_path, n_fft=args.nfft, hop=args.hop, task='train', n_spk=args.nspk,
                                     n_spk_prob=args.spkprob == 1, llen=args.tr_len, max_len=args.max_len, env=args.env,
                                     use_pit=args.use_pit),
                              batch_size=args.batch_size, shuffle=True, **kwargs)

    validation_loader = DataLoader(loader(validation_data_path, n_fft=args.nfft, hop=args.hop, task='valid',
                                          llen=args.te_len, max_len=args.max_len, env=args.env, use_pit=args.use_pit),
                                   batch_size=args.batch_size, shuffle=False, **kwargs)

    print("#\tCreating Model...")

    model = InfoComMask(n_fft=args.nfft, hop=args.hop, kernel=(args.kf, args.kt),
                        causal=args.causal == 1, layers=args.layers, stacks=args.stacks, use_pit=args.use_pit)

    if args.gpus > 1 and num_gpu > 1:
        model = nn.DataParallel(model)

    if args.load is not None:
        print(f"#\tLoading model {args.load}")
        load_path = f"{base_dir}/logs2/{args.load}/model_weights.pt"
        model.load_state_dict(torch.load(load_path))

    s = 0
    for param in model.parameters():
        s += np.product(param.size())

    print(f"#\tNumber of parameters: {s}")
    print("#" * 99)
    optimizer = optim.Adam(model.parameters(), lr=args.lr)

    params = {'nfft': args.nfft,
              'hop': args.hop,
              'kf': args.kf,
              'kt': args.kt,
              'stacks': args.stacks,
              'nspk': args.nspk,
              'layers': args.layers,
              'causal': args.causal,
              'spkprob': args.spkprob,
              'tr_len': args.tr_len,
              'te_len': args.te_len,
              'max_len': args.max_len,
              'noise_train': args.noise_train,
              'frame_train': args.frame_train,
              'noise_test': args.noise_test,
              'frame_test': args.frame_test,
              'dataset': training_data_path,
              }

    with open(f"{logdir}/architecture.json", 'w') as arch_file:
        json.dump(params, arch_file)

    # description
    description = json.dumps(params)
    logger.text_summary('model', description)

    # main
    training_loss = []
    validation_loss = []
    decay_cnt = 0

    for epoch in range(1, args.epochs + 1):
        if args.cuda:
            model.cuda()

        training_loss.append(train(model, train_loader, optimizer, epoch, args))
        validation_loss.append(test(model, validation_loader, epoch, args))

        logger.scalar_summary('Train loss over epochs', training_loss[-1], epoch)
        logger.scalar_summary('Test loss over epochs', validation_loss[-1], epoch)

        if validation_loss[-1] == np.min(validation_loss):
            with open(f"{val_save}", 'wb') as f:
                torch.save(model.cpu().state_dict(), f)
                print(f"Saving best model to {val_save}")
        print("=" * 99)
        decay_cnt += 1


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='biss')
    parser.add_argument('--batch-size', type=int, default=6)
    parser.add_argument('--epochs', type=int, default=15)
    parser.add_argument('--cuda', action='store_true', default=True)
    parser.add_argument('--lr', type=float, default=1e-3)
    parser.add_argument('--noise', type=float, default=0.0, help='noise envelope')
    parser.add_argument('--seed', type=int, default=42, help='random seed')
    parser.add_argument('--gpus', type=int, default=1)
    parser.add_argument('--nfft', type=int, default=512)
    parser.add_argument('--hop', type=int, default=125)
    parser.add_argument('--kf', type=int, default=3)
    parser.add_argument('--kt', type=int, default=3)
    parser.add_argument('--stacks', type=int, default=2)
    parser.add_argument('--nspk', type=int, default=2)
    parser.add_argument('--layers', type=int, default=6)
    parser.add_argument('--causal', type=int, default=0)
    parser.add_argument('--spkprob', type=int, default=0)
    parser.add_argument('--tr_len', type=int, default=20000)
    parser.add_argument('--te_len', type=int, default=5000)
    parser.add_argument('--max_len', type=int, default=32000)

    parser.add_argument('--env', type=str, default='std')

    parser.add_argument('--noise_train', type=float, default=0.)
    parser.add_argument('--frame_train', type=int, default=1)
    parser.add_argument('--noise_test', type=float, default=0.)
    parser.add_argument('--frame_test', type=int, default=1)

    parser.add_argument('--use_pit', type=int, default=0)
    parser.add_argument('--chain', type=int, default=0)

    parser.add_argument('--exp_name', type=str, default='biss_default')
    parser.add_argument('--load', type=str, default=None)
    parser.add_argument('--bab', type=int, default=0)

    arguments, _ = parser.parse_known_args()
    arguments.cuda = arguments.cuda and torch.cuda.is_available()

    main(arguments)
