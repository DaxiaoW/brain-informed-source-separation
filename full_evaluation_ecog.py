import csv
import json
import pickle as pkl
import warnings

import numpy as np
import torch
from sep_eval import sep_eval as se
from tqdm import tqdm

from model_stft import InfoComMask

CONST = 10 * np.log10(np.exp(1))
warnings.simplefilter(action='ignore', category=FutureWarning)


class Evaluator(object):
    def __init__(self):
        
        self.model = None
        self.models = []
        
        self.measures = []
        self.name_measure = []
        
        self.datasets = []
        self.name_dataset = []
        self.corrs_t = []
        self.corrs_f = []

    def add_model(self, name):
        self.models.append(name)
    
    def add_measure(self, measure, name=None):
        if name is None:
            name = 'measure{}'.format(len(self.measures))
        self.name_measure.append(name)
        self.measures.append(measure)
    
    def add_dataset(self, mix, env, s1, name=None, corrs_t=None, corrs_f=None):
        if name is None:
            name = 'model{}'.format(len(self.models))
        if corrs_t is not None:
            self.corrs_t.append(corrs_t)
        else:
            self.corrs_t.append(np.zeros((len(mix))))

        if corrs_f is not None:
            self.corrs_f.append(corrs_f)
        else:
            self.corrs_f.append(np.zeros((len(mix))))

        self.name_dataset.append(name)
        self.datasets.append({'mix': mix, 'env': env, 's1': s1})

    @staticmethod
    def single_test(model, mix, env):
        # doing batches of 4
        batch_size = 1
        out = []
        for i in tqdm(range(0, len(mix), batch_size)):
            _out = model([mix[i:i + batch_size], env[i:i + batch_size]])
            _out = _out.cpu().data.numpy()
            out.append(_out)
        out = np.vstack(out)
        return out

    @staticmethod
    def load_info_model(load, base_dir='/Data/Dropbox/PhD/Projects/brain-informed-source-separation'):
        json_dir = base_dir + '/logs2/' + load
        with open(json_dir + '/architecture.json', 'r') as fff:
            p = json.load(fff)
            load_path = json_dir

            model = InfoComMask(n_fft=p['nfft'], kernel=(p['kf'], p['kt']), causal=p['causal'],
                                layers=p['layers'], stacks=p['stacks'], verbose=False)

            model.load_state_dict(torch.load(load_path + '/model_weights.pt'))
            _ = model.eval()
            return model, p

    def evaluate(self, file_path='./out.csv', mode='w'):
        
        with open(file_path, mode) as csv_file:
            file_writer = csv.writer(csv_file, delimiter=',')
            header = ['model', "nfft", "hop", "kernel1", "kernel2", "stacks", "nspk", "layers", "causal", "spkprob",
                      "tr_len", "te_len", "max_len", "noise_train", "frame_train", "noise_test", "frame_test",
                      "dataset_path", "dataset_name", "corr_t", "corr_f"] + self.name_measure
            file_writer.writerow(header)
            for model in self.models:
                model_row = [model]
                
                print("Loading {}".format(model))
                _model, _p = self.load_info_model(model)

                for k, v in _p.items():
                    model_row.append(v)

                for dataset, name_dataset, corrs_t, corrs_f in zip(self.datasets, self.name_dataset, self.corrs_t, self.corrs_f):
                    dataset_row = [name_dataset]
                    _out = self.single_test(_model, dataset['mix'], dataset['env'])
                    _s1 = dataset['s1'].cpu().data.numpy()
                    collected_measures = []
                    for measure, name_measure in tqdm(zip(self.measures, self.name_measure), desc='sdr'):
                        collected_measures.append(measure(_out, _s1))
                    # separate
                    res = np.zeros((len(self.measures), len(collected_measures[0]), 3))
                    for j, m in enumerate(collected_measures):
                        for i, val in enumerate(m):
                            res[j, i, 0] = val
                            res[j, i, 1] = corrs_t[i]
                            res[j, i, 2] = corrs_f[i]
                    for i in range(res.shape[1]):
                        _t = []
                        _t.append(res[0, i, 1])
                        _t.append(res[0, i, 2])
                        for j in range(res.shape[0]):
                            _t.append(res[j, i, 0])
                        file_writer.writerow(model_row + dataset_row + _t)
                del _model  # for RAM


def main():
    print("Loading data...")
    evaluator = Evaluator()

    for sub in ['045_LIJ120', '026_LIJ110', '018_LIJ107']:
        data = pkl.load(open(f'ecog_{sub}_v4.pkl', 'rb'))
        mix = data['mix']
        s1 = data['s1']
        s2 = data['s2']
        raw_gt1 = data['raw_gt1']
        raw_gt2 = data['raw_gt2']
        real_gt1 = data['real_gt1']
        real_gt2 = data['real_gt2']

        corrs11 = [np.corrcoef(a, b)[0, 1] for a, b in zip(raw_gt1, real_gt1)]
        corrs22 = [np.corrcoef(a, b)[0, 1] for a, b in zip(raw_gt2, real_gt2)]
        corrs12 = [np.corrcoef(a, b)[0, 1] for a, b in zip(raw_gt1, real_gt2)]
        corrs21 = [np.corrcoef(a, b)[0, 1] for a, b in zip(raw_gt2, real_gt1)]

        print("REAL test MIX: {}".format(mix.shape))
        print("REAL test S1: {}".format(s1.shape))
        print("REAL test ENV1: {}".format(real_gt1.shape))

        evaluator.add_dataset(mix, raw_gt1, s1, name=f'raw test 1 {sub}', corrs_t=corrs11, corrs_f=corrs12)
        evaluator.add_dataset(mix, raw_gt2, s2, name=f'raw test 2 {sub}', corrs_t=corrs22, corrs_f=corrs21)

    # NC
    evaluator.add_model('201908214513_ICM_noise_40_32_std_125_sep_NC')

    # C
    evaluator.add_model('201908052205_ICM_noise_50_40_std_125_sep_C')

    evaluator.add_measure(se.sdr, name='sdr')
    evaluator.add_measure(se.stoi, name='stoi')
    evaluator.evaluate(file_path='./final_evaluation_ecog_with_corrs_v5.csv')
    

if __name__ == "__main__":
    main()
